import { UpdateComponent } from './pages/update/update.component';
import { ListComponent } from './pages/list/list.component';
import { UserRoutingModule } from './user-routing.module';
import { NgModule } from '@angular/core';
import { CreateComponent } from './pages/create/create.component';
import { SharedModule } from '@shared/shared.module';
import { ShowComponent } from './pages/show/show.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from '../../../app.module';
import { HttpClient } from '@angular/common/http';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    SharedModule,
    UserRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgbDatepickerModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  exports: [],
  declarations: [
    ListComponent,
    CreateComponent,
    UpdateComponent,
    ShowComponent
  ],
  providers: [],
})
export class UserModule { }
