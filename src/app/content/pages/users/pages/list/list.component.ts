import { UserResponse } from './../../../../../shared/models/user.d';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../../core/service/user.service';

@Component({
  selector: 'icv-list',
  templateUrl: 'list.component.html',
  styleUrls: ['list.component.scss']
})

export class ListComponent implements OnInit {
  public loading = true;
  public listUsers: UserResponse[] = [];
  public letters: string = '';
  constructor(private _userService: UserService) { this.loadUsers(); }

  ngOnInit() { }

  private loadUsers(): void {
    this.loading = true;
    this._userService.getAll().subscribe(
      response => {
        this.listUsers = response;
        setTimeout(() => {
          this.loading = false;
        }, 2000);
      }, error => {
        setTimeout(() => {
          this.loading = false;
        }, 2000);
        console.log(error);
      }
    );
  }

  private get filterList(): UserResponse[] {
    return this.listUsers.filter(obj => obj.name ? obj.name.toLowerCase().includes(this.letters.toLowerCase()) : false || obj.birthdate.toLowerCase().includes(this.letters.toLowerCase()));
  }
}