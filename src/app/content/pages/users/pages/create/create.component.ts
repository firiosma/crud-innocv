import { UserService } from './../../../../../core/service/user.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'icv-create',
  templateUrl: 'create.component.html',
  styleUrls: ['create.component.scss'],
})

export class CreateComponent implements OnInit {
  public form: FormGroup;
  public step: number = 0;
  public loading = false;

  constructor(private _fb: FormBuilder, private _userService: UserService, private _router: Router, private _translate: TranslateService) {
    this.form = this.create
  }


  ngOnInit() { }

  public get create(): FormGroup {
    return this._fb.group({
      Name: new FormControl(null, Validators.required),
      Birthdate: new FormControl(null),
      day: new FormControl(null, [Validators.required, Validators.max(31), Validators.minLength(1), Validators.min(1), (control: AbstractControl): { [key: string]: any } | null => {
        if (control.value) {
          if (this.form.get('month').valid) {
            if (control.value == 31) {
              if (['04', '06', '09', '11'].some(obj => obj === this.form.get('month').value)) {
                return { invalidDate: true };
              }
            }
            if (control.value > 29 && this.form.get('month').value === '02') {
              return { invalidDate: true };
            }
          }
          if (this.form.get('month').hasError('invalidDate')) {
            this.form.get('month').setErrors(null);
          }
          return null;
        }
      }]),
      month: new FormControl("", [Validators.required, (control: AbstractControl): { [key: string]: any } | null => {
        if (control.value) {
          if (this.form.get('day').valid) {
            if (this.form.get('day').value == 31) {
              if (['04', '06', '09', '11'].some(obj => obj === control.value)) {
                return { invalidDate: true };
              }
            }
            if (this.form.get('day').value > 29 && control.value === '02') {
              return { invalidDate: true };
            }
          }
          if (this.form.get('day').hasError('invalidDate')) {
            this.form.get('day').setErrors(null);
          }
          return null;
        }
      }]),
      year: new FormControl(null, [Validators.required, Validators.max((new Date).getFullYear() - 1), Validators.min(1900), Validators.minLength(4)]),
    });
  }

  public submit() {
    if (this.form.valid) {
      const request = { Name: this.form.get('Name').value, Birthdate: new Date() }
      request.Birthdate.setFullYear(this.form.get('year').value);
      request.Birthdate.setMonth(Number(this.form.get('month').value) - 1);
      request.Birthdate.setDate(Number(this.form.get('day').value) - 1);
      this._userService.create(request).subscribe(
        response => {
          this._router.navigate(['/pages/user/list']);
        }, error => {
          console.log(error);
          this.swalError();
        }
      );
    } else {
      this.form.markAllAsTouched();
    }
  }

  public next() {
    console.log(this.form);
    if (this.form.get('Name').valid) {
      this.step = 1;
    } else {
      this.form.get('Name').markAsTouched();
    }
  }

  public back() {
    this._router.navigate(['/pages/user/list']);
  }

  public isInvalid(form: FormGroup, key: string) {
    return form.get(key).touched && !form.get(key).valid;
  }

  public isValid(form: FormGroup, key: string) {
    return form.get(key).touched && form.get(key).valid;
  }
  public hasError(form: FormGroup, key: string, error: string) {
    return form.get(key).touched && form.get(key).hasError(error);
  }

  private swalError() {
    Swal.fire({
      type: 'error',
      title: 'Oops...',
      text: this._translate.instant('shared.error'),
    })
  }
}