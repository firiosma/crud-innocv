import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../../../core/service/user.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'icv-show',
  templateUrl: 'show.component.html',
  styleUrls: ['./show.component.scss']
})

export class ShowComponent implements OnInit {
  public loading = true;
  public user: any;
  constructor(private _userService: UserService, private _activatedRoute: ActivatedRoute, private _router: Router) { this.getUser(); }

  ngOnInit() { }

  public edit() {
    this._router.navigate(['/pages/user/edit/' + this._activatedRoute.snapshot.paramMap.get('id')]);
  }
  public back() {
    this._router.navigate(['/pages/user/list']);
  }

  public getUser() {
    this._userService.get(this._activatedRoute.snapshot.paramMap.get('id')).subscribe(
      response => {
        this.user = response;
        setTimeout(() => {
          this.loading = false;
        }, 2000);
      },
      error => {
        setTimeout(() => {
          this.loading = false;
        }, 2000);
      }
    );
  }

  public get zodiac(): string {
    if (this.user) {
      const date = new Date(this.user.birthdate);
      const dia = date.getDay();
      const mes = date.getMonth() + 1;
      if ((dia >= 21 && mes == 3) || (dia <= 20 && mes == 4))
        return 'aries';
      if ((dia >= 24 && mes == 9) || (dia <= 23 && mes == 10))
        return 'libra';
      if ((dia >= 21 && mes == 4) || (dia <= 21 && mes == 5))
        return 'taurus';
      if ((dia >= 24 && mes == 10) || (dia <= 22 && mes == 11))
        return 'scorpio';
      if ((dia >= 22 && mes == 5) || (dia <= 21 && mes == 6))
        return 'gemini';
      if ((dia >= 23 && mes == 11) || (dia <= 21 && mes == 12))
        return 'sagittarius';
      if ((dia >= 21 && mes == 6) || (dia <= 23 && mes == 7))
        return 'cancer';
      if ((dia >= 22 && mes == 12) || (dia <= 20 && mes == 1))
        return 'capricorn';
      if ((dia >= 24 && mes == 7) || (dia <= 23 && mes == 8))
        return 'leo';
      if ((dia >= 21 && mes == 1) || (dia <= 19 && mes == 2))
        return 'aquarius';
      if ((dia >= 24 && mes == 8) || (dia <= 23 && mes == 9))
        return 'virgo';
      if ((dia >= 20 && mes == 2) || (dia <= 20 && mes == 3))
        return 'piscis';
    } else {
      return null
    }
  }
}