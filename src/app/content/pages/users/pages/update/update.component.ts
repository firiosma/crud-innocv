import { UserService } from './../../../../../core/service/user.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, AbstractControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from '@shared/models/user';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'icv-update',
  templateUrl: 'update.component.html',
  styleUrls: ['update.component.scss']
})

export class UpdateComponent implements OnInit {
  public form: FormGroup;
  public step: number = 0;
  public loading = true;

  constructor(private _fb: FormBuilder, private _userService: UserService, private _activatedRoute: ActivatedRoute, private _router: Router, private _translate: TranslateService) { this.getUser(); }

  ngOnInit() { }

  public getUser() {
    this._userService.get(this._activatedRoute.snapshot.paramMap.get('id')).subscribe(
      response => {
        this.form = this.create(response);
        setTimeout(() => {
          this.loading = false;
        }, 2000);
      },
      error => {
        setTimeout(() => {
          this.loading = false;
        }, 2000);
      }
    );
  }

  public create(user: any): FormGroup {
    const date = new Date(user.birthdate);
    return this._fb.group({
      Id: new FormControl(user.id),
      Name: new FormControl(user.name, Validators.required),
      Birthdate: new FormControl(null),
      day: new FormControl(String(date.getDate() + 1), [Validators.required, Validators.max(31), Validators.minLength(1), (control: AbstractControl): { [key: string]: any } | null => {
        if (control.value && this.form) {
          if (this.form.get('month').valid) {
            if (control.value == 31) {
              if (['04', '06', '09', '11'].some(obj => obj === this.form.get('month').value)) {
                return { invalidDate: true };
              }
            }
            if (control.value > 29 && this.form.get('month').value === '02') {
              return { invalidDate: true };
            }
          }
          if (this.form.get('month').hasError('invalidDate')) {
            this.form.get('month').setErrors(null);
          }
          return null;
        }
      }]),
      month: new FormControl(date.getMonth() > 9 ? String(date.getMonth() + 1) : String("0" + (date.getMonth() + 1)), [Validators.required, (control: AbstractControl): { [key: string]: any } | null => {
        if (control.value && this.form) {
          if (this.form.get('day').valid) {
            if (this.form.get('day').value == 31) {
              if (['04', '06', '09', '11'].some(obj => obj === control.value)) {
                return { invalidDate: true };
              }
            }
            if (this.form.get('day').value > 29 && control.value === '02') {
              return { invalidDate: true };
            }
          }
          if (this.form.get('day').hasError('invalidDate')) {
            this.form.get('day').setErrors(null);
          }
          return null;
        }
      }]),
      year: new FormControl(String(date.getFullYear()), [Validators.required, Validators.max((new Date).getFullYear() - 1), Validators.min(1900), Validators.minLength(4)]),
    });
  }

  public submit() {
    if (this.form.valid) {
      const request: User = { Name: this.form.get('Name').value, Birthdate: new Date(), Id: Number(this._activatedRoute.snapshot.paramMap.get('id')) }
      request.Birthdate.setFullYear(this.form.get('year').value);
      request.Birthdate.setMonth(Number(this.form.get('month').value) - 1);
      request.Birthdate.setDate(Number(this.form.get('day').value) - 1);
      this._userService.update(request).subscribe(
        response => {
          this._router.navigate(['/pages/user/list']);
        }, error => {
          console.log(error);
          this.swalError();
        }
      );
    } else {
      this.form.markAllAsTouched();
    }
  }

  public back() {
    this._router.navigate(['/pages/user/list']);
  }

  public next() {
    console.log(this.form);
    if (this.form.get('Name').valid) {
      this.step = 1;
    } else {
      this.form.get('Name').markAsTouched();
    }
  }


  public isInvalid(form: FormGroup, key: string) {
    return form.get(key).touched && !form.get(key).valid;
  }

  public isValid(form: FormGroup, key: string) {
    return form.get(key).touched && form.get(key).valid;
  }
  public hasError(form: FormGroup, key: string, error: string) {
    return form.get(key).touched && form.get(key).hasError(error);
  }

  private swalError() {
    Swal.fire({
      type: 'error',
      title: 'Oops...',
      text: this._translate.instant('shared.error'),
    })
  }
}