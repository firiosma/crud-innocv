import { CreateComponent } from './pages/create/create.component';
import { ListComponent } from './pages/list/list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShowComponent } from './pages/show/show.component';
import { UpdateComponent } from './pages/update/update.component';

const routes: Routes = [
  {
    path: '', redirectTo: 'list', pathMatch: 'full'
  },
  {
    path: 'list', component: ListComponent
  },
  {
    path: 'create', component: CreateComponent
  },
  {
    path: 'show/:id', component: ShowComponent
  },
  {
    path: 'edit/:id', component: UpdateComponent
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [],
})
export class UserRoutingModule { }