import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardUserListComponent } from '@shared/components/card-user-list/card-user-list.component';
import { HoroscopePipe } from '@shared/pipe/horoscope.pipe';
import { LoaderComponent } from '@shared/components/loader/loader.component';
import { RadomUserPipe } from '@shared/pipe/img-random.pipe';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpLoaderFactory } from '../app.module';
import { HttpClient } from '@angular/common/http';
import { OnlyNumber } from '@directives/ony-number.directive';
import { LetterOnlyDirective } from '@directives/only-letters.directive';


@NgModule({
  imports: [
    CommonModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  exports: [
    //#region Module
    CommonModule,
    //#endregion
    //#region Components
    LoaderComponent,
    CardUserListComponent,
    //#endregion
    //#region Pipe
    HoroscopePipe,
    RadomUserPipe,
    //#endregion
    //#region Directives
    OnlyNumber,
    LetterOnlyDirective
    //#endregion
  ],
  declarations: [
    //#region Components
    CardUserListComponent,
    LoaderComponent,
    //#endregion
    //#region Pipe
    HoroscopePipe,
    RadomUserPipe,
    //#endregion
    //#region Directives
    OnlyNumber,
    LetterOnlyDirective
    //#endregion
  ],
  providers: [],
})
export class SharedModule { }
