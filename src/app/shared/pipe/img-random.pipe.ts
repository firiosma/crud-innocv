import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'randomUser'
})
export class RadomUserPipe implements PipeTransform {
  transform(value: any, ...args: any[]): any {
    switch (value % 14) {
      case 1:
        return 'assets/img/users/300_1.jpg';
      case 2:
        return 'assets/img/users/300_2.jpg';
      case 3:
        return 'assets/img/users/300_3.jpg';
      case 4:
        return 'assets/img/users/300_4.jpg';
      case 5:
        return 'assets/img/users/300_5.jpg';
      case 6:
        return 'assets/img/users/300_6.jpg';
      case 7:
        return 'assets/img/users/300_7.jpg';
      case 8:
        return 'assets/img/users/300_8.jpg';
      case 9:
        return 'assets/img/users/300_9.jpg';
      case 10:
        return 'assets/img/users/300_10.jpg';
      case 11:
        return 'assets/img/users/300_11.jpg';
      case 12:
        return 'assets/img/users/300_12.jpg';
      case 13:
        return 'assets/img/users/300_13.jpg';
      default:
        return 'assets/img/users/300_14.jpg';
    }
  }
}