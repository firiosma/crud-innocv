import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'horoscope'
})

export class HoroscopePipe implements PipeTransform {
  transform(value: any, ...args: any[]): any {
    if (value) {
      const date = new Date(value);
      const dia = date.getDay();
      const mes = date.getMonth() + 1;
      if ((dia >= 21 && mes == 3) || (dia <= 20 && mes == 4))
        return 'assets/img/aries.svg';
      if ((dia >= 24 && mes == 9) || (dia <= 23 && mes == 10))
        return 'assets/img/libra.svg';
      if ((dia >= 21 && mes == 4) || (dia <= 21 && mes == 5))
        return 'assets/img/taurus.svg';
      if ((dia >= 24 && mes == 10) || (dia <= 22 && mes == 11))
        return 'assets/img/scorpio.svg';
      if ((dia >= 22 && mes == 5) || (dia <= 21 && mes == 6))
        return 'assets/img/gemini.svg';
      if ((dia >= 23 && mes == 11) || (dia <= 21 && mes == 12))
        return 'assets/img/sagittarius.svg';
      if ((dia >= 21 && mes == 6) || (dia <= 23 && mes == 7))
        return 'assets/img/cancer.svg';
      if ((dia >= 22 && mes == 12) || (dia <= 20 && mes == 1))
        return 'assets/img/capricorn.svg';
      if ((dia >= 24 && mes == 7) || (dia <= 23 && mes == 8))
        return 'assets/img/leo.svg';
      if ((dia >= 21 && mes == 1) || (dia <= 19 && mes == 2))
        return 'assets/img/aquarius.svg';
      if ((dia >= 24 && mes == 8) || (dia <= 23 && mes == 9))
        return 'assets/img/virgo.svg';
      if ((dia >= 20 && mes == 2) || (dia <= 20 && mes == 3))
        return 'assets/img/piscis.svg';
    } else {
      return null;
    }
  }
}