import { UserService } from './../../../core/service/user.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2'
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'icv-card-user-list',
  templateUrl: 'card-user-list.component.html',
  styleUrls: ['./card-user-list.component.scss']
})

export class CardUserListComponent implements OnInit {
  @Input() birthdate: string = '01/01';
  @Input() tittle: string = 'Nombre';
  @Input() id: number;
  @Input() index;
  @Output() refresh: EventEmitter<void> = new EventEmitter();
  public actions = false;

  constructor(private _router: Router, private translate: TranslateService, private _userService: UserService) { }

  ngOnInit() { }

  public show(): void {
    this._router.navigate(['/pages/user/show/' + this.id]);
  }

  public edit(): void {
    this._router.navigate(['/pages/user/edit/' + this.id]);
  }

  public delete(): void {
    Swal.fire({
      title: this.translate.instant('shared.delete.title'),
      text: this.translate.instant('shared.delete.text'),
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: this.translate.instant('shared.delete.confirm'),
      cancelButtonText: this.translate.instant('shared.delete.cancel'),
    }).then((result) => {
      if (result.value) {
        Swal.fire({
          title: this.translate.instant('shared.delete.delete'),
          text: this.translate.instant('shared.delete.please'),
          allowOutsideClick: false,
          allowEscapeKey: false,
          allowEnterKey: false,
          onOpen: () => {
            Swal.showLoading()
          }
        });
        this._userService.delete(this.id).subscribe(
          response => {
            Swal.close();
            this.refresh.emit();
          },
          error => {
            Swal.close();
            this.swalError();
          }
        );
      } else if (result.dismiss === Swal.DismissReason.cancel) { }
    })
  }

  private swalError() {
    Swal.fire({
      type: 'error',
      title: 'Oops...',
      text: this.translate.instant('shared.error'),
    })
  }
}