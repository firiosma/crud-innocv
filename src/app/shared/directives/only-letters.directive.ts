import { Directive, HostListener, ElementRef } from '@angular/core';

@Directive({ selector: 'input[letterOnly]' })
export class LetterOnlyDirective {
  constructor(private _el: ElementRef) { }

  @HostListener('input', ['$event']) onInputChange(event) {
    const initalValue = this._el.nativeElement.value;
    this._el.nativeElement.value = initalValue.replace(/[^a-zA-ZáéíóúÁÉÍÓÚ\s]*/g, '');
    if (initalValue !== this._el.nativeElement.value) {
      event.stopPropagation();
    }
  }
}