export interface User {
  Id?: number;
  Name: string;
  Birthdate: Date;
}

export interface UserResponse {
  id?: number;
  name: string;
  birthdate: string;
}