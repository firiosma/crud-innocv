import { TestBed, async, getTestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { Router } from '@angular/router';
class RouterStub {
  navigateByUrl = jasmine.createSpy('navigateByUrl');
}

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        CoreModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  xit('should navigate', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    let router: Router = getTestBed().get(Router);
    expect(router.navigateByUrl).toHaveBeenCalledWith('pages/user/list');
  });
});
