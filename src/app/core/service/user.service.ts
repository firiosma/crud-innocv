import { User, UserResponse } from '../../shared/models/user.d';
import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from '@environments/environment';
import { catchError } from 'rxjs/operators';


const API_ENDPOINT_BASE = 'user'

@Injectable()
export class UserService {
  constructor(private httpClient: HttpClient) { }

  public getAll(): Observable<UserResponse[]> {
    return this.httpClient
      .get<UserResponse[]>(environment.baseUrl.concat(API_ENDPOINT_BASE))
      .pipe(catchError(error => this.handleError(error)));
  }

  public get(id: string): Observable<UserResponse> {
    return this.httpClient
      .get<UserResponse>(environment.baseUrl.concat(API_ENDPOINT_BASE).concat('/' + id))
      .pipe(catchError(error => this.handleError(error)));
  }

  public create(body: User): Observable<UserResponse> {
    return this.httpClient
      .post<UserResponse>(environment.baseUrl.concat(API_ENDPOINT_BASE), body)
      .pipe(catchError(error => this.handleError(error)));
  }

  public delete(id: number): Observable<void> {
    return this.httpClient
      .delete<void>(environment.baseUrl.concat(API_ENDPOINT_BASE).concat('/' + id))
      .pipe(catchError(error => this.handleError(error)));
  }

  public update(body: User): Observable<UserResponse> {
    return this.httpClient
      .put<UserResponse>(environment.baseUrl.concat(API_ENDPOINT_BASE), body)
      .pipe(catchError(error => this.handleError(error)));
  }

  private handleError(error: HttpErrorResponse) {
    return throwError(error.error);
  }
}