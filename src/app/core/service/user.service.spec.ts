import { UserService } from './user.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { User, UserResponse } from '@shared/models/user';

describe('Service: UserService', () => {
  let userService: UserService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    // Isolate Configuration
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule
      ],
      providers: [
        UserService,
      ]
    });

    httpMock = TestBed.get(HttpTestingController);
    userService = new UserService(TestBed.get(HttpClient));
  });

  it('#UserService should be created', () => {
    expect(userService).toBeTruthy();
  });

  it('#getAll should return Code 200', async () => {
    const responseSucess = [{ "name": "Prueba", "birthdate": "2018-08-02T04:56:56.285", "id": 2676 }, { "name": "Oscar Manuel", "birthdate": "1994-09-12T19:03:12.074", "id": 2677 }, { "name": "Oscar Manuel", "birthdate": "1994-08-11T19:05:58.976", "id": 2678 }, { "name": "PEPE", "birthdate": "2019-07-19T03:12:05.124", "id": 2679 }, { "name": "Oscar", "birthdate": "2010-08-11T03:11:10.209", "id": 2686 }, { "name": "Alfredo", "birthdate": "1995-01-22T02:43:09.338", "id": 2687 }, { "name": "Prueba", "birthdate": "2017-08-02T04:54:12.96", "id": 2689 }, { "name": "Adasdas", "birthdate": "2019-07-12T13:41:33", "id": 2690 }, { "name": "Oscar", "birthdate": "1996-01-30T12:30:25.284", "id": 2691 }]

    userService.getAll().subscribe((response) => {
      expect(response).toEqual(responseSucess); // Mock Http Response
    });

    const req = httpMock // Http Request Expected
      .expectOne(req => req.method === 'GET' && req.url === "http://hello-world.innocv.com/api/user");

    req.flush(responseSucess, { status: 200, statusText: 'Ok' }); // Expected Body

    httpMock.verify(); // Verify Http Request (Always Needed)
  });

  it('#get should return Code 200', async () => {
    const id = "2677";
    const responseSucess = { "name": "Oscar Manuel", "birthdate": "1994-09-12T19:03:12.074", "id": 2677 };

    userService.get(id).subscribe((response) => {
      expect(response).toEqual(responseSucess); // Mock Http Response
    });

    const req = httpMock // Http Request Expected
      .expectOne(req => req.method === 'GET' && req.url === ("http://hello-world.innocv.com/api/user/" + id));

    req.flush(responseSucess, { status: 200, statusText: 'Ok' }); // Expected Body

    httpMock.verify(); // Verify Http Request (Always Needed)
  });

  it('#delete should return Code 204', async () => {
    const id = 2677;
    userService.delete(id).subscribe((response) => {
    });

    const req = httpMock // Http Request Expected
      .expectOne(req => req.method === 'DELETE' && req.url === ("http://hello-world.innocv.com/api/user/" + id));

    req.flush('', { status: 204, statusText: 'No Content' }); // Expected Body

    httpMock.verify(); // Verify Http Request (Always Needed)
  });

  it('#update should return Code 204', async () => {
    const user: User = { "Name": "Oscar Manuel", "Birthdate": new Date("1994-09-12T19:03:12.074"), "Id": 2677 };
    userService.update(user).subscribe((response) => {
    });

    const req = httpMock // Http Request Expected
      .expectOne(req => req.method === 'PUT' && req.url === ("http://hello-world.innocv.com/api/user"));

    req.flush('', { status: 204, statusText: 'No Content' }); // Expected Body

    httpMock.verify(); // Verify Http Request (Always Needed)
  });

  it('#create should return Code 204', async () => {
    const user: User = { "Name": "Oscar Manuel", "Birthdate": new Date("1994-09-12T19:03:12.074") };
    userService.create(user).subscribe((response) => {
    });

    const req = httpMock // Http Request Expected
      .expectOne(req => req.method === 'POST' && req.url === ("http://hello-world.innocv.com/api/user"));

    req.flush('', { status: 204, statusText: 'No Content' }); // Expected Body

    httpMock.verify(); // Verify Http Request (Always Needed)
  });
});