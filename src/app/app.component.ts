import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'icv-root',
  template: "  <icv-header></icv-header>" +
    "<router-outlet></router-outlet>"
})
export class AppComponent { }
